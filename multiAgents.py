# multiAgents.py
# --------------
# Licensing Information: Please do not distribute or publish solutions to this
# project. You are free to use and extend these projects for educational
# purposes. The Pacman AI projects were developed at UC Berkeley, primarily by
# John DeNero (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# For more info, see http://inst.eecs.berkeley.edu/~cs188/sp09/pacman.html

from util import manhattanDistance
from game import Directions
import random, util

from game import Agent

class ReflexAgent(Agent):
	"""
		A reflex agent chooses an action at each choice point by examining
		its alternatives via a state evaluation function.

		The code below is provided as a guide.	You are welcome to change
		it in any way you see fit, so long as you don't touch our method
		headers.
	"""


	def getAction(self, gameState):
		"""
		You do not need to change this method, but you're welcome to.

		getAction chooses among the best options according to the evaluation function.

		Just like in the previous project, getAction takes a GameState and returns
		some Directions.X for some X in the set {North, South, West, East, Stop}
		"""
		# Collect legal moves and successor states
		legalMoves = gameState.getLegalActions()

		# Choose one of the best actions
		scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
		bestScore = max(scores)
		bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
		chosenIndex = random.choice(bestIndices) # Pick randomly among the best

		"Add more of your code here if you want to"

		return legalMoves[chosenIndex]

	def evaluationFunction(self, currentGameState, action):
		"""
		Design a better evaluation function here.
		
		The evaluation function takes in the current and proposed successor
		GameStates (pacman.py) and returns a number, where higher numbers are better.
		
		The code below extracts some useful information from the state, like the
		remaining food (oldFood) and Pacman position after moving (newPos).
		newScaredTimes holds the number of moves that each ghost will remain
		scared because of Pacman having eaten a power pellet.

		Print out these variables to see what you're getting, then combine them
		to create a masterful evaluation function.
		"""
		# Useful information you can extract from a GameState (pacman.py)
		successorGameState = currentGameState.generatePacmanSuccessor(action)
		#print successorGameState
		#%%%%
		#% . %
		#%.G.%
		#% . %
		#%. .%
		#%   %
		#%  .%
		#%   %
		#% <.%

		newPos = successorGameState.getPacmanPosition()
		#print newpos = (1, 2)

		oldFood = currentGameState.getFood()
		#print oldFood
		#FFFFF
		#FFTFF
		#FTFTF
		#FFTFF
		#etc

		newGhostStates = successorGameState.getGhostStates()

		#for each in newGhostStates:
		#	print each
		#	util.pause()
		#Ghost: (x,y)=(2, 7), Stop
		#<What>: (x,y)=<x coord, y coord), <last move (stop, east, north, etc)>
		newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
		#print newScaredTimes
		#no power pellets on map = [0]
		
		#exponential, close to ghost run away. far away, get pellets
		"*** YOUR CODE HERE ***"
		curGhost = 1
		closestGhostDistance=100000000
		for ghost in newGhostStates:
			curGhost = manhattanDistance(ghost.getPosition(),newPos)
			if curGhost < closestGhostDistance:
				closestGhostDistance = curGhost
				tempGhost = ghost

		#no ghosts on map 
		if closestGhostDistance == 100000000:
			pass

		#pacman is deciding if he should try to eat the ghost. SPOILER: NO
		if closestGhostDistance == 0:
			return -100000000
		elif closestGhostDistance > 5:
			closestGhostDistance = 5
		howScared = 1.0/closestGhostDistance

		foodPositions = oldFood.asList()
		curFood = 1
		closestFoodDistance=100000000
		for food in foodPositions:
			curFood = manhattanDistance(food,newPos)
			if curFood < closestFoodDistance:
				closestFoodDistance = curFood

		#make pacman really hungry, but not hungry enough that he will eat a ghost
		if closestFoodDistance == 0:
			howHungry = 1
		else:
			howHungry = 1.0/closestFoodDistance
		#if food is closer than ghost is, return will be positive
		#if ghost is closer than food is, return will be negative
		#if a ghost and food are on the same square, this will be hugely negative
		return howHungry-howScared

def scoreEvaluationFunction(currentGameState):
	"""
		This default evaluation function just returns the score of the state.
		The score is the same one displayed in the Pacman GUI.

		This evaluation function is meant for use with adversarial search agents
		(not reflex agents).
	"""
	return currentGameState.getScore()

class MultiAgentSearchAgent(Agent):
	"""
		This class provides some common elements to all of your
		multi-agent searchers.	Any methods defined here will be available
		to the MinimaxPacmanAgent, AlphaBetaPacmanAgent & ExpectimaxPacmanAgent.

		You *do not* need to make any changes here, but you can if you want to
		add functionality to all your adversarial search agents.	Please do not
		remove anything, however.

		Note: this is an abstract class: one that should not be instantiated.	It's
		only partially specified, and designed to be extended.	Agent (game.py)
		is another abstract class.
	"""

	def __init__(self, evalFn = 'scoreEvaluationFunction', depth = '2'):
		self.index = 0 # Pacman is always agent index 0
		self.evaluationFunction = util.lookup(evalFn, globals())
		self.depth = int(depth)

class MinimaxAgent(MultiAgentSearchAgent):
	"""
		Your minimax agent (question 2)
	"""

	def getAction(self, gameState):
		"""
			Returns the minimax action from the current gameState using self.depth
			and self.evaluationFunction.

			Here are some method calls that might be useful when implementing minimax.

			gameState.getLegalActions(agentIndex):
				Returns a list of legal actions for an agent
				agentIndex=0 means Pacman, ghosts are >= 1

			Directions.STOP:
				The stop direction, which is always legal

			gameState.generateSuccessor(agentIndex, action):
				Returns the successor game state after an agent takes an action

			gameState.getNumAgents():
				Returns the total number of agents in the game
		"""
		"*** YOUR CODE HERE ***"
		def minimax(gameState, depth, agentIndex):

			if gameState.isWin() or gameState.isLose() or depth == 0:
				return self.evaluationFunction(gameState);
			
			if agentIndex == 0:
				bestScore = -float("inf")
				listOfActions = gameState.getLegalActions(0);
				listOfActions.remove(Directions.STOP);
				for action in listOfActions:
					bestScore = max(bestScore, minimax(gameState.generateSuccessor(0, action), depth, 1));
				return bestScore
			else:
				bestScore = float("inf")
				listOfActions = gameState.getLegalActions(agentIndex);
				if agentIndex != gameState.getNumAgents() - 1:
					for action in listOfActions:
						bestScore = min(bestScore, minimax(gameState.generateSuccessor(agentIndex, action), depth, agentIndex + 1));
				else:
					for action in listOfActions:
						bestScore = min(bestScore, minimax(gameState.generateSuccessor(agentIndex, action), depth - 1, 0));	
				return bestScore

		bestActionForPacMan = Directions.STOP
		possibleActions = gameState.getLegalActions(0)
		scoreFromActions = -float("inf")
		for action in possibleActions:
			formerScore = scoreFromActions
			scoreFromActions = max(scoreFromActions, minimax(gameState.generateSuccessor(0, action), self.depth, 1))
			if scoreFromActions > formerScore:
				formerScore = scoreFromActions;
				bestActionForPacMan = action
		# print formerScore;
		return bestActionForPacMan

class AlphaBetaAgent(MultiAgentSearchAgent):
	"""
		Your minimax agent with alpha-beta pruning (question 3)
	"""

	def getAction(self, gameState):
		"""
			Returns the minimax action using self.depth and self.evaluationFunction
		"""
		"*** YOUR CODE HERE ***"
		# util.raiseNotDefined()
		self._myBestMoveVar_ = Directions.STOP;
		self.alphaBetaMinimax(gameState, 0, self.depth, self.depth, -float("inf"), float("inf"));
		return self._myBestMoveVar_;
		
	def alphaBetaMinimax(self, state, agentIndex, depth, initDepth, alpha, beta):
		# check if at search bound
		if(depth == 0 or state.isWin() or state.isLose()):
			return self.evaluationFunction(state);
		
		#get the list of legal actions
		actionList = state.getLegalActions(agentIndex);
		
		#if this is the last ghost to move decrement depth
		if(agentIndex == state.getNumAgents() - 1):
			depth -= 1;
		
		#if this is pacman
		if (agentIndex == 0):
			for action in actionList:
				result = self.alphaBetaMinimax(state.generateSuccessor(agentIndex, action), (agentIndex+1)%state.getNumAgents(), depth, initDepth, alpha, beta);
				if result > alpha:
					alpha = result
					if depth == initDepth:
						self._myBestMoveVar_ = action;
				if alpha >= beta:
					return alpha;
			return alpha;
		else:#else its a ghost
			for action in actionList:
				result = self.alphaBetaMinimax(state.generateSuccessor(agentIndex, action), (agentIndex+1)%state.getNumAgents(), depth, initDepth, alpha, beta);
				if result < beta:
					beta = result;
				if beta <= alpha:
					return beta;
			return beta;



class ExpectimaxAgent(MultiAgentSearchAgent):
	"""
		Your expectimax agent (question 4)
	"""

	def getAction(self, gameState):
		"""
			Returns the expectimax action using self.depth and self.evaluationFunction

			All ghosts should be modeled as choosing uniformly at random from their
			legal moves.
		"""
		"*** YOUR CODE HERE ***"
			#function to get expectation value from the list of actions
		def expectectation(gameState, depth, agentIndex):
			if gameState.isWin() or gameState.isLose() or depth == 0:
				return self.evaluationFunction(gameState);

			possibleActions = gameState.getLegalActions(agentIndex)
			numberOfPossibleActions = len(possibleActions)
			value = 0
			
			for action in possibleActions:
				if(agentIndex == gameState.getNumAgents() - 1): #if last ghose agent
					value += maximumValue(gameState.generateSuccessor(agentIndex, action), depth - 1)
				else:	#if it is not last ghost agent
					value += expectectation(gameState.generateSuccessor(agentIndex, action), depth, agentIndex + 1)
				#divide value of every action by number of possible moves to give every legal action same proportion
			value = value / numberOfPossibleActions
			
			return value

			#function to get maximum value of action
		def maximumValue(gameState, depth):
			if gameState.isWin() or gameState.isLose() or depth == 0:
				return self.evaluationFunction(gameState);
			
			possibleActions = gameState.getLegalActions(0)
			score = -float("inf")

			for action in possibleActions:
				formerScore = scoreFromActions
				score = max(score, expectectation(gameState.generateSuccessor(0, action), depth, 1))

			return score

		bestActionForPacMan = Directions.STOP
		possibleActions = gameState.getLegalActions(0)
		scoreFromActions = -float("inf")

		for action in possibleActions:
			formerScore = scoreFromActions
			scoreFromActions = max(scoreFromActions, expectectation(gameState.generateSuccessor(0, action), self.depth, 1))
			
			if scoreFromActions > formerScore:
				formerScore = scoreFromActions;
				bestActionForPacMan = action
		
			# print formerScore;
		return bestActionForPacMan
def betterEvaluationFunction(currentGameState):
	"""
		Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
		evaluation function (question 5).

		DESCRIPTION: <write something here so we know what you did>
	"""
	"*** YOUR CODE HERE ***"
	# util.raiseNotDefined()
	
	# # Useful information you can extract from a GameState (pacman.py)
	# successorGameState = currentGameState.generatePacmanSuccessor(action)
	# newPos = successorGameState.getPacmanPosition()
	# oldFood = currentGameState.getFood()
	# newGhostStates = successorGameState.getGhostStates()
	# newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
	
	#get some data from the game state
	pacman = currentGameState.getPacmanPosition();
	ghostList = currentGameState.getGhostStates();
	foodList = currentGameState.getFood().asList();
	
	#get the points of the ghost's
	ghostPointList = [];
	for ghost in ghostList:
		ghostPointList.append(ghost.getPosition());
	
	#find closest ghost and food
	closestFood = findClosestToPacman(currentGameState, foodList);
	closestGhost = findClosestToPacman(currentGameState, ghostPointList);
	
	#if this state contains no food then we win... so return infinity
	if(len(foodList) == 0):
		return float("inf");
	
	#calculate a value based on closest food and number of food left
	value = round(5.0/(closestFood+1),3) + round(1000.0/len(foodList), 3);
	
	#if we are close to a ghost we should factor that in
	if(closestGhost <= 2):
		#if we are next to a ghost RUN!!!
		if(closestGhost < 1):
			return -float("inf");
		
		return -1000*closestGhost + value;
		
	return value;

def findClosestToPacman(state, endPointList):
	#if were at the point there is no distance to it
	if(state.getPacmanPosition() in endPointList):
		return 0;
		
	#set up the lists
	seenList = [state.getPacmanPosition()];
	que = util.Queue();
	que.push([state.getPacmanPosition(), 0]);
	moveMap = {Directions.NORTH:(0,1), Directions.EAST:(1,0), Directions.SOUTH:(0,-1), Directions.WEST:(-1,0) };
	walls = state.getWalls();
	
	while(not que.isEmpty()):
		point, dist = que.pop();
		
		for action,shift in moveMap.iteritems():
			#get the next point
			nextPoint = (point[0]+shift[0], point[1]+shift[1]);
			
			#since walls always border the map checking for walls guarantees that we will never
			#get a point that is out of bounds
			if(nextPoint not in seenList and not walls[nextPoint[0]][nextPoint[1]]):
				# if we found an end point
				if(nextPoint in endPointList):
					return dist+1;
				
				seenList.append(nextPoint);
				que.push([nextPoint, dist+1]);
	
	return 0;
	

# Abbreviation
better = betterEvaluationFunction

class ContestAgent(MultiAgentSearchAgent):
	"""
		Your agent for the mini-contest
	"""
	
	def getAction(self, gameState):
		"""
			Returns an action.	You can use any method you want and search to any depth you want.
			Just remember that the mini-contest is timed, so you have to trade off speed and computation.

			Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
			just make a beeline straight towards Pacman (or away from him if they're scared!)
		"""
		"*** YOUR CODE HERE ***"
		# util.raiseNotDefined()
		self._myDepth_ = 5;
		self._myBestMoveVar_ = Directions.STOP;
		self.alphaBetaMinimax(gameState, 0, self._myDepth_, -float("inf"), float("inf"));
		return self._myBestMoveVar_;
		
	
	def alphaBetaMinimax(self, state, agentIndex, depth, alpha, beta):
		# check if at search bound
		if(depth == 0 or state.isWin() or state.isLose()):
			return self.EvaluationFunction(state);
		
		#get the list of legal actions
		actionList = state.getLegalActions(agentIndex);
		
		if(Directions.STOP in actionList):
			actionList.remove(Directions.STOP);
		
		#if this is the last ghost to move decrement depth
		if(agentIndex == state.getNumAgents() - 1):
			depth -= 1;
		
		#if this is pacman
		if (agentIndex == 0):
			for action in actionList:
				result = self.alphaBetaMinimax(state.generateSuccessor(agentIndex, action), (agentIndex+1)%state.getNumAgents(), depth, alpha, beta);
				if result > alpha:
					alpha = result
					if depth == self._myDepth_:
						self._myBestMoveVar_ = action;
				if alpha >= beta:
					return alpha;
			return alpha;
		else:#else its a ghost
			for action in actionList:
				result = self.alphaBetaMinimax(state.generateSuccessor(agentIndex, action), (agentIndex+1)%state.getNumAgents(), depth, alpha, beta);
				if result < beta:
					beta = result;
				if beta <= alpha:
					return beta;
			return beta;

	def EvaluationFunction(self, currentGameState):
		"""
			Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable
			evaluation function (question 5).

			DESCRIPTION: <write something here so we know what you did>
		"""
		"*** YOUR CODE HERE ***"
		# util.raiseNotDefined()
		
		# # Useful information you can extract from a GameState (pacman.py)
		# successorGameState = currentGameState.generatePacmanSuccessor(action)
		# newPos = successorGameState.getPacmanPosition()
		# oldFood = currentGameState.getFood()
		# newGhostStates = successorGameState.getGhostStates()
		# newScaredTimes = [ghostState.scaredTimer for ghostState in newGhostStates]
		
		#get some data from the game state
		pacman = currentGameState.getPacmanPosition();
		ghostList = currentGameState.getGhostStates();
		foodList = currentGameState.getFood().asList();
		
		#get the points of the ghost's
		ghostPointList = [];
		for ghost in ghostList:
			ghostPointList.append(ghost.getPosition());
		
		#find closest ghost and food
		closestFood = findClosestToPacman(currentGameState, foodList + currentGameState.getCapsules());
		closestGhost = findClosestToPacman(currentGameState, ghostPointList);
		
		# #if this state contains no food then we win... so return infinity
		if(len(foodList) == 0):
			return float("inf");
		
		#calculate a value based on closest food and number of food left
		value = round(2.0/(closestFood+1),3) + round(2000.0/(len(foodList)+len(currentGameState.getCapsules())), 3);
		
		#if we are close to a ghost we should factor that in
		if(closestGhost <= 2):
			#if we are next to a ghost RUN!!!
			if(closestGhost == 0):
				return -float("inf");
			elif(closestGhost == 1):
				return -9999999;
			
			return -1000*closestGhost + value;
			
		return value;
